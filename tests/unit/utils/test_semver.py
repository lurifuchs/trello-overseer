import unittest

from utils.semver import Semver

class TestSemver(unittest.TestCase):

    def test_get_version(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertEqual(semver.get_version(), version)

    def test_get_sortable_version(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertEqual(semver.get_sortable_version(True), '010203')

    def test_get_major(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertEqual(semver.get_major(), '1')
        self.assertEqual(semver.get_major(True), '01')

    def test_get_minor(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertEqual(semver.get_minor(), '2')
        self.assertEqual(semver.get_minor(True), '02')

    def test_get_patch(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertEqual(semver.get_patch(), '3')
        self.assertEqual(semver.get_patch(True), '03')

    def test_is_stable(self):
        version = '1.2.3'
        semver = Semver(version)
        self.assertTrue(semver.is_stable())

        version = '1.2.3-beta'
        semver = Semver(version)
        self.assertFalse(semver.is_stable())
