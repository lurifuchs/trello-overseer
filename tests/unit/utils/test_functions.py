import unittest

from utils import utils_functions
from utils.semver import Semver

class TestFunctions(unittest.TestCase):

    def test_root_path(self):
        pass

    def test_dotenv(self):
        loaded = utils_functions.dotenv()
        self.assertTrue(loaded)

    def test_dotenv_custom_path(self):
        loaded = utils_functions.dotenv('/tmp')
        self.assertFalse(loaded)

    def test_get_platforms(self):
        platforms = utils_functions.get_platforms(str(utils_functions.root_path()) + '/platforms')
        self.assertEqual(len(platforms), 2)

    def test_compare_versions(self):
        lower = Semver('1.0.0')
        higher = Semver('1.1.0')
        res = utils_functions.compare_versions(higher, lower, 'major')
        self.assertEqual(res, 0)

        res = utils_functions.compare_versions(higher, lower, 'minor')
        self.assertEqual(res, 1)

        res = utils_functions.compare_versions(lower, higher)
        self.assertEqual(res, -1)
