import unittest, os
from unittest.mock import Mock, patch

from platforms.drupal import Drupal

class TestDrupal(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.env = patch.dict(os.environ, {
            'DRUPAL_TRELLO_BOARD_ID': 'drupal_board_id',
            'DRUPAL_TRELLO_LIST_ID': 'drupal_list_id',
            'DRUPAL_TRELLO_UPDATE_LIST_ID': 'drupal_update_list_id'
        })
        cls.env.start()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.env.stop()

    def setUp(self):
        self.drupal = Drupal()

    def test_get_name(self):
        self.assertEqual(self.drupal.get_name(), 'Drupal')

    @patch('trello_overseer.platforms.drupal.requests.get')
    def test_get_latest_version(self, mock_get):
        json_content = [
            {'name': '8.2.13'},
            {'name': '10.4.5'},
            {'name': '9.9.2'}
        ]

        mock_response = Mock(status_code=200)
        mock_response.json.return_value = json_content
        mock_get.return_value = mock_response
        latest_version = self.drupal.get_latest_version()
        self.assertEqual(latest_version.get_version(), '10.4.5')

    @patch('trello_overseer.platforms.drupal.requests.get')
    def test_get_cards_500(self, mock_get):
        mock_response = Mock(status_code=500)
        mock_get.return_value = mock_response
        latest_version = self.drupal.get_latest_version()
        self.assertIsNone(latest_version)

    def test_get_trello_board_id(self):
        id = self.drupal.get_trello_board_id()
        self.assertEqual(id, 'drupal_board_id')

    def test_get_trello_list_id(self):
        id = self.drupal.get_trello_list_id()
        self.assertEqual(id, 'drupal_list_id')

    def test_get_trello_update_list_id(self):
        id = self.drupal.get_trello_update_list_id()
        self.assertEqual(id, 'drupal_update_list_id')