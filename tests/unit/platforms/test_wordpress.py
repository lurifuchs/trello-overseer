import unittest, os
from unittest.mock import Mock, patch

from platforms.wordpress import Wordpress

class TestWordpress(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.env = patch.dict(os.environ, {
            'WORDPRESS_TRELLO_BOARD_ID': 'wordpress_board_id',
            'WORDPRESS_TRELLO_LIST_ID': 'wordpress_list_id',
            'WORDPRESS_TRELLO_UPDATE_LIST_ID': 'wordpress_update_list_id'
        })
        cls.env.start()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.env.stop()

    def setUp(self):
        self.wordpress = Wordpress()

    def test_get_name(self):
        self.assertEqual(self.wordpress.get_name(), 'Wordpress')

    @patch('trello_overseer.platforms.wordpress.requests.get')
    def test_get_latest_version(self, mock_get):
        json_content = [
            {'name': '5.2.13'},
            {'name': '6.4.5'},
            {'name': '5.9.2'}
        ]

        mock_response = Mock(status_code=200)
        mock_response.json.return_value = json_content
        mock_get.return_value = mock_response
        latest_version = self.wordpress.get_latest_version()
        self.assertEqual(latest_version.get_version(), '6.4.5')

    @patch('trello_overseer.platforms.wordpress.requests.get')
    def test_get_cards_500(self, mock_get):
        mock_response = Mock(status_code=500)
        mock_get.return_value = mock_response
        latest_version = self.wordpress.get_latest_version()
        self.assertIsNone(latest_version)

    def test_get_trello_board_id(self):
        id = self.wordpress.get_trello_board_id()
        self.assertEqual(id, 'wordpress_board_id')

    def test_get_trello_list_id(self):
        id = self.wordpress.get_trello_list_id()
        self.assertEqual(id, 'wordpress_list_id')

    def test_get_trello_update_list_id(self):
        id = self.wordpress.get_trello_update_list_id()
        self.assertEqual(id, 'wordpress_update_list_id')