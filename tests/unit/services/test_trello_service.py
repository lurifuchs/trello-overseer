import unittest, os, requests
from unittest.mock import Mock, MagicMock, patch

from services.trello_service import TrelloService

class TestTrelloService(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.env = patch.dict(os.environ, {
            'TRELLO_API_KEY': 'test',
            'TRELLO_API_TOKEN': 'test'
        })
        cls.env.start()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.env.stop()

    def setUp(self):
        self.trello_service = TrelloService()

    def test_get_base_uri(self):
        self.assertEqual(self.trello_service.get_base_uri(), TrelloService.base_url)

    @patch('trello_overseer.services.trello_service.requests.get')
    def test_get_cards_500(self, mock_get):
        mock_response = MagicMock(status_code=500)
        mock_get.return_value = mock_response
        response = self.trello_service.get_cards('123456789')
        self.assertFalse(response)

    @patch('trello_overseer.services.trello_service.requests.get')
    def test_get_cards_exception(self, mock_get):
        mock_get.side_effect = requests.exceptions.RequestException
        response = self.trello_service.get_cards('123456789')
        self.assertFalse(response)

    @patch('trello_overseer.services.trello_service.requests.get')
    def test_get_cards_success(self, mock_get):
        json_content = [{
                'idMembers': ['1234'],
                'member': {
                    'fullName': 'TESTNAME'
                }
            },
            {
                'idMembers': ['5678']
            }
        ]

        mock_response = Mock(status_code=200)
        mock_response.json.return_value = json_content
        mock_get.return_value = mock_response
        cards = self.trello_service.get_cards('123456789')
        self.assertEqual(len(cards), 2)

    @patch('trello_overseer.services.trello_service.requests.get')
    def test_get_cards_missing_request_data(self, mock_get):
        json_content = [{
                'member': {
                    'fullName': 'TESTNAME'
                }
            },
            {
                'idMembers': ['5678']
            }
        ]

        mock_response = Mock(status_code=200)
        mock_response.json.return_value = json_content
        mock_get.return_value = mock_response
        cards = self.trello_service.get_cards('123456789')
        self.assertEqual(len(cards), 2)
        self.assertEqual(cards[0]['member']['fullName'], 'Unassigned')

    def test_move_card_to_update_list(self):
        self.trello_service.move_card_to_update_list({'id': '1234', 'name': 'TEST'}, '123456789')
        # Assert something

    @patch('trello_overseer.services.trello_service.requests.put')
    def test_move_card_to_update_list_exception(self, mock_put):
        mock_put.side_effect = requests.exceptions.RequestException
        response = self.trello_service.move_card_to_update_list({'id': '1234', 'name': 'TEST'}, '123456789')
        self.assertFalse(response)

    def test_move_card_from_update_list(self):
        self.trello_service.move_card_from_update_list({'id': '1234', 'name': 'TEST'}, '123456789')
        # Assert something

    @patch('trello_overseer.services.trello_service.requests.put')
    def test_move_card_from_update_list_exception(self, mock_put):
        mock_put.side_effect = requests.exceptions.RequestException
        response = self.trello_service.move_card_from_update_list({'id': '1234', 'name': 'TEST'}, '123456789')
        self.assertFalse(response)

    @patch('trello_overseer.services.trello_service.requests.get')
    def test_get_card_member_exception(self, mock_get):
        mock_get.side_effect = requests.exceptions.RequestException
        response = self.trello_service.get_card_member({'idMembers': '1234'})
        self.assertFalse(response)