import os, importlib, logging

from dotenv import load_dotenv
from pathlib import Path

from utils.semver import Semver
from platforms.abstract_platform import AbstractPlatform

def root_path() -> os.PathLike:
    return Path(os.path.dirname(os.path.realpath(__file__))).parent

def dotenv(path=None):
    if path != None:
        dotenv_path = Path(path + '/.env')
    else:
        dotenv_path = Path(str(root_path().parent) + '/.env')

    if os.path.isfile(dotenv_path):
        load_dotenv()
        return True
    else:
        logging.error('No .env file found in ' + str(dotenv_path) + '. .env is only available in docker --target dev. Check your docker command or create a .env.')
        return False

def get_platforms(path: str) -> list[AbstractPlatform]:
    # Get all platform classes by parsing all files in platforms folder
    platform_modules = os.listdir(str(root_path()) + '/platforms')
    ignore = {'__init__.py', '__pycache__', '.DS_Store', 'abstract_platform.py'}

    platforms = []
    for platform_module in platform_modules:
        if platform_module not in ignore:
            platform_basename = platform_module.removesuffix('.py')
            module_path = 'platforms.' + platform_basename
            module = importlib.import_module(module_path)
            platform_class = getattr(module, platform_basename.title())
            platform = platform_class()
            platforms.append(platform)

    return platforms

def compare_versions(compare: Semver, compare_against: Semver, granularity: str='patch'):
    '''
    Compare two versions and return:
    -1 if compare is older than compare_against
    0 if compare is the same as compare_against
    1 if compare is newer than compare_against
    '''
    if (compare.get_sortable_version(granularity) == compare_against.get_sortable_version(granularity)):
        return 0
    elif (compare.get_sortable_version(granularity) < compare_against.get_sortable_version(granularity)):
        return -1
    else:
        return 1