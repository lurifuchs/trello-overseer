import re

class Semver:

    def __init__(self, version_string: str) -> str:
        search_result = re.search(r'(\d+)\.(\d+)\.?(\d+)?-?(.+)?', version_string)

        self.version_string = version_string
        self.version = None

        if search_result:
            major, minor, patch, suffix = search_result.groups()
            self.version = list(filter(None, [major, minor, patch, suffix]))

    def get_version(self):
        return self.version_string

    def get_sortable_version(self, granularity: str='patch') -> str:
        if (granularity == 'major'):
            return self.get_major(True)
        elif (granularity == 'minor'):
            return self.get_major(True) + self.get_minor(True)
        else:
            return self.get_major(True) + self.get_minor(True) + self.get_patch(True)

    def get_major(self, leading_zero:bool=False) -> str:
        major_version = self.version[0] if len(self.version) >= 1 else 0
        return str(major_version).zfill(2) if leading_zero else major_version

    def get_minor(self, leading_zero:bool=False)-> str:
        minor_version = self.version[1] if len(self.version) >= 2 else 0
        return str(minor_version).zfill(2) if leading_zero else minor_version

    def get_patch(self, leading_zero:bool=False)-> str:
        patch_version = self.version[2] if len(self.version) >= 3 else 0
        return str(patch_version).zfill(2) if leading_zero else patch_version

    def is_stable(self) -> bool:
        has_suffix = True if len(self.version) >= 4 else False
        return not has_suffix