import requests, os, datetime, logging

from services.trello_service import TrelloService
from utils.semver import Semver
from utils import utils_functions

def logging_setup(log_file: str = '/tmp/trello_overseer.log'):
    '''
    Setup logging
    '''

    log_dir = '/tmp'
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s',
        filename=log_file,
    )

def main() -> None:
    logging.info('Trello Overseer is running...')
    # Run application
    utils_functions.dotenv()

    platforms_dir = str(utils_functions.root_path()) + '/platforms'
    platforms = utils_functions.get_platforms(platforms_dir)
    for platform in platforms:
        if not platform.is_valid():
            message = f"Platform {platform.get_name()} is not valid"
            logging.error(message)
            print(message)
            continue

        # Get latest platform version
        latest_version = platform.get_latest_version()

        if latest_version is None:
            logging.error(f"Error getting latest version for {platform.get_name()}")
            continue

        # Get Trello cards
        trello_service = TrelloService()
        cards = trello_service.get_cards(platform.get_trello_board_id())

        # Loop trello cards and verify that title has version number
        cards_per_member = {}
        if cards:
            for card in cards:
                version = Semver(card['name'])

                if utils_functions.compare_versions(version, latest_version, 'minor') == -1:
                    if card['idList'] == platform.get_trello_list_id():
                        trello_service.move_card_to_update_list(card, platform.get_trello_update_list_id())

                        if card['member']:
                            cards_per_member[card['member']['fullName']] = []
                            name = card['name'].split('-')[0] if card['name'].split('-') else card['name']
                            cards_per_member[card['member']['fullName']].append(name)
                else:
                    if card['idList'] == platform.get_trello_update_list_id():
                        trello_service.move_card_from_update_list(card, platform.get_trello_list_id())

        # Notifiy Slack of sites that need updating
        message = platform.get_name() + ' ' + latest_version.get_version() + ' is available. Please update your sites.'
        message += '\n\nSites that need updating:'

        for member, cards in cards_per_member.items():
            message += '\n' + member + ': ' + ', '.join(cards)

        time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        if cards_per_member:
            logging.info(f"{time} {platform.get_name()} ({latest_version.get_version()}): Moved {str(len(cards_per_member.items()))} cards to update")

            # Send message to slack if there are sites that need updating
            if os.getenv('SLACK_WEBHOOK'):
                requests.request(
                    'POST',
                    os.getenv('SLACK_WEBHOOK'),
                    json={'text': message}
            )
        else:
            logging.info(f"{time} {platform.get_name()} ({latest_version.get_version()}): No actions")

if __name__ == '__main__':
    print('Trello Overseer is running...')
    logging_setup()
    main()

def lambda_handler(event, context):
    '''
    AWS Lambda handler.
    This is the entry point for the AWS Lambda function.
    '''

    logging_setup('/tmp/trello_overseer_lambda.log')
    main()

    return {
        'event': event,
        'context': context,
    }