from abc import ABC, abstractmethod

from utils.semver import Semver

class AbstractPlatform(ABC):

    @abstractmethod
    def get_name(self) -> str:
        pass

    @abstractmethod
    def get_latest_version(self) -> Semver|None:
        pass

    @abstractmethod
    def get_trello_board_id(self) -> str:
        pass

    @abstractmethod
    def get_trello_list_id(self) -> str:
        pass

    @abstractmethod
    def get_trello_update_list_id(self) -> str:
        pass

    def is_valid(self):
        if self.get_trello_board_id() and self.get_trello_list_id() and self.get_trello_update_list_id():
            return True
        else:
            return False