import os, requests, logging

from utils.semver import Semver
from platforms.abstract_platform import AbstractPlatform

class Wordpress(AbstractPlatform):

    endpoint_url = 'https://api.github.com/repos/wordpress/wordpress/tags'

    def get_name(self) -> str:
        return 'Wordpress'

    def get_latest_version(self) -> Semver|None:
        '''
        Get the latest version of WordPress Core from github releases.
        '''
        response = requests.get(self.endpoint_url)

        if response.status_code == 200:
            versions = []

            for tag in response.json():
                version = Semver(tag['name'])

                if version is not None and version.is_stable():
                    versions.append(version)

            versions.sort(key=lambda version:version.get_sortable_version(), reverse=True)
            return versions[0] # Return latest version
        else:
            logging.error(f"Error getting latest version for {self.get_name()}: {response.status_code} {response.text}")
            return None

    def get_trello_board_id(self) -> str:
        key = 'WORDPRESS_TRELLO_BOARD_ID'
        return os.environ[key] if key in os.environ else None

    def get_trello_list_id(self) -> str:
        key = 'WORDPRESS_TRELLO_LIST_ID'
        return os.environ[key] if key in os.environ else None

    def get_trello_update_list_id(self) -> str:
        key = 'WORDPRESS_TRELLO_UPDATE_LIST_ID'
        return os.environ[key] if key in os.environ else None