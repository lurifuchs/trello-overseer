import os, requests, logging

class TrelloService:
    base_url = 'https://api.trello.com/1'

    def __init__(self):
        self.api_key = os.environ['TRELLO_API_KEY']
        self.api_token = os.environ['TRELLO_API_TOKEN']
        self.headers = {
            'Accept': 'application/json'
        }
        self.auth = {
            'key': self.api_key,
            'token': self.api_token
        }

    @staticmethod
    def get_base_uri():
        return TrelloService.base_url

    def get_cards(self, board_id: str) -> list[dict[str, str]] | bool:
        '''Get cards from Trello API'''

        url = self.base_url + '/boards/' + board_id + '/cards'

        try:
            response = requests.get(
                url,
                headers=self.headers,
                params=self.auth
            )

            if response.status_code == 200:
                cards = response.json()

                for card in cards:
                    # Add member data to card
                    member = self.get_card_member(card)
                    card['member'] = member.copy() if member else {'fullName': 'Unassigned'}

                return cards
            else:
                logging.error('Error in trello_service.get_cards (board_id ' + board_id + '): (' + str(response.status_code) + ') ' + response.text)
                return False
        except requests.exceptions.RequestException as e:
            logging.error('Error in trello_service.get_cards: ' + str(e))
            return False

    def move_card_to_update_list(self, card, trello_update_list_id: str) -> bool:
        '''Move card to update list'''

        url = self.base_url + '/cards/' + card['id']
        params = {'idList': trello_update_list_id} | self.auth

        try:
            requests.put(
                url,
                headers=self.headers,
                params=params
            )

            return True
        except Exception as e:
            logging.error('Error in trello_service.move_card_to_list: ' + card['name'] + ' could not be moved: ' + str(e))
            return False

    def move_card_from_update_list(self, card, trello_list_id: str) -> bool:
        '''Move card from update list'''

        url = self.base_url + '/cards/' + card['id']
        params = {'idList': trello_list_id} | self.auth

        try:
            requests.put(
                url,
                headers=self.headers,
                params=params
            )

            return True
        except requests.exceptions.RequestException as e:
            logging.error('Error in trello_service.move_card_to_list: ' + card['name'] + ' could not be moved: ' + str(e))
            return False

    def get_card_member(self, card):
        '''Get member'''

        if 'idMembers' not in card or len(card['idMembers']) == 0:
            return False

        url = self.base_url + '/members/' + card['idMembers'][0]

        try:
            response = requests.get(
                url,
                headers=self.headers,
            )

            return response.json()
        except requests.exceptions.RequestException as e:
            logging.error('Error in trello_service.__get_card_member: ' + str(e))
            return False
