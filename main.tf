terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    bucket         = "lurifuchs-terraform-states"
    key            = "trello-overseer/terraform.tfstate"
    region         = "eu-north-1"
    encrypt        = true
    dynamodb_table = "lurifuchs-terraform-states"
  }
}

provider "aws" {
  region = var.aws_region
}

###
# ECR
###

#
resource "aws_ecr_repository" "trello_overseer_ecr_repo" {
  name = "trello-overseer"
}

#
data "aws_ecr_image" "trello_overseer_image" {
  repository_name = "trello-overseer"
  image_tag       = "latest"
}

###
# Lambda
###

# Lambda function
resource "aws_lambda_function" "trello_overseer" {
  depends_on       = [null_resource.docker_packaging, aws_cloudwatch_log_group.lambda_log_group]
  architectures    = ["x86_64"]
  function_name    = "TrelloOverseerFunction"
  timeout          = 5
  image_uri        = "${aws_ecr_repository.trello_overseer_ecr_repo.repository_url}:latest"
  package_type     = "Image"
  role             = aws_iam_role.lambda_role.arn
  source_code_hash = trimprefix(data.aws_ecr_image.trello_overseer_image.id, "sha256:")

  environment {
    variables = {
      TRELLO_API_KEY=""
      TRELLO_API_SECRET=""
      TRELLO_API_TOKEN=""
      DRUPAL_TRELLO_BOARD_ID=""
      DRUPAL_TRELLO_LIST_ID=""
      DRUPAL_TRELLO_UPDATE_LIST_ID=""
      WORDPRESS_TRELLO_BOARD_ID=""
      WORDPRESS_TRELLO_LIST_ID=""
      WORDPRESS_TRELLO_UPDATE_LIST_ID=""
      SLACK_WEBHOOK=""
    }
  }

  lifecycle {
    ignore_changes = [
      environment.0.variables,
    ]
  }
}

resource "aws_lambda_permission" "trello_overseer_cron_permission" {
  statement_id  = "TrelloOverseerCronPermission"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.trello_overseer.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.trello_overseer_cron.arn
}

# Lambda IAM
resource "aws_iam_role" "lambda_role" {
  name = "TrelloOverseerLambdaRole"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = [
            "lambda.amazonaws.com",
            "logs.amazonaws.com"
          ]
        }
      }
    ]
  })
}

#
resource "aws_iam_policy" "function_logging_policy" {
  name = "function-logging-policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        Action : [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Effect : "Allow",
        Resource : "arn:aws:logs:*:*:*"
      }
    ]
  })
}

#
resource "aws_iam_role_policy_attachment" "function_logging_policy_attachment" {
  role       = aws_iam_role.lambda_role.id
  policy_arn = aws_iam_policy.function_logging_policy.arn
}

###
# CloudWatch
###

# Log group for Lambda function.
resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "TrelloOverseerLogs"
  retention_in_days = 7
}

# Triggers Lambda function automatically.
resource "aws_cloudwatch_event_rule" "trello_overseer_cron" {
  name                = "trello-overseer-cron"
  schedule_expression = "cron(0 8 * * ? *)"
}

resource "aws_cloudwatch_event_target" "trello_overseer_cron" {
  rule = aws_cloudwatch_event_rule.trello_overseer_cron.name
  arn  = aws_lambda_function.trello_overseer.arn
}

###
# Docker
###

# This resource is used to build the Docker image and push it to ECR on initial deployment.check "name" {
# ECR image is updated on commits to the main branch and push to ECR with GitLab CI/CD.
resource "null_resource" "docker_packaging" {
  depends_on = [aws_ecr_repository.trello_overseer_ecr_repo]

  triggers = {
    "run_at" = timestamp()
  }

  provisioner "local-exec" {
    command = <<EOF
      aws ecr get-login --region ${var.aws_region}
      aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${var.aws_account_id}.dkr.ecr.eu-north-1.amazonaws.com
      docker build -t trello-overseer . --target prod
      docker tag trello-overseer:latest ${aws_ecr_repository.trello_overseer_ecr_repo.repository_url}:latest
      docker push ${aws_ecr_repository.trello_overseer_ecr_repo.repository_url}:latest
    EOF
  }
}
