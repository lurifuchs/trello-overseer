# What is this?
Trello overseer finds the latest Drupal and WordPress releases and move Trello cards (our way of tracking the state of clients sites as 040.se) to the to-update list/column if there's a new minor version.

The setup for this project is a python application in a docker container that's deployed to AWS ECR and then run on AWS Lambda with a CloudWatch rule triggering it once a day. The infrastructure is provisioned using Terraform.

# Todo
* Make sure code handles missing env variables.
* Move some dependencies to dev dependencies.
* Better structure in main.tf file.
* Write more tests.

# Get project up and running locally
1. Create virtual environment (venv) using `python3 -m venv env`.
2. Activate venv with `source env/bin/activate`.
3. Install poetry.
3. Install requirements using `poetry install`.
4. And finally run app using `python3 trello_overseer/main.py`.

# .env
Trello variables for lists need to be found through the API, I couldn't find them in the UI.

# Docker
* `docker build -t trello-overseer . --target dev|prod`
* `docker run trello-overseer`

# Upload to AWS ECR
This is automated in GitLab CI/CD but can be run manually if needed.
* `aws ecr get-login-password --region eu-north-1 | docker login --username AWS --password-stdin 533267358898.dkr.ecr.eu-north-1.amazonaws.com`
* `docker tag trello-overseer:latest 533267358898.dkr.ecr.eu-north-1.amazonaws.com/trello-overseer:latest`
* `docker push 533267358898.dkr.ecr.eu-north-1.amazonaws.com/trello-overseer:latest`

Test AWS lambda handler locally
* `docker run --platform linux/amd64 -p 9000:8080 trello-overseer:test`
* `curl "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'`

# Provision infrastructure
Use Terraform

# Unittests
* `pytest --cov-report=term-missing --cov=trello_overseer tests`