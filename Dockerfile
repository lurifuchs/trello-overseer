FROM --platform=linux/amd64 public.ecr.aws/lambda/python:3.12 AS base
COPY ./trello_overseer /app/trello_overseer
COPY poetry.lock pyproject.toml /app/trello_overseer/

WORKDIR /app/trello_overseer
RUN pip install poetry
RUN poetry export --output=requirements.txt
RUN pip install -r requirements.txt

FROM base AS dev
COPY .env /app
ENTRYPOINT [ "python", "main.py" ]
CMD ["trello_overseer/main.lambda_handler"]

FROM base AS prod
ENTRYPOINT [ "python", "main.py" ]
CMD ["trello_overseer/main.lambda_handler"]
